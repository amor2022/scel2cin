SUMMARY="搜狗词库转换工具"
DESCRIPTION="一个用于转换搜狗输入法词库(.scel)文件的图形界面工具，支持将词库转换为文本格式(.txt)和中文输入法通用格式(.cin)。"
HOMEPAGE="https://gitcode.com/amor2022/scel2cin"
COPYRIGHT="2023"
LICENSE="MIT"
REVISION="1"
ARCH="any"

PROVIDES="
	scel2cin = $portVersion
	app:Scel2Cin = $portVersion
	"
REQUIRES="
	haiku
	python3
	lib:libpython3.10
	"

BUILD_REQUIRES="
	haiku_devel
	python3_devel
	"

BUILD_PREREQUIRES="
	cmd:python3
	cmd:scons
	"

BUILD()
{
	scons $jobArgs
}

INSTALL()
{
	scons install
}