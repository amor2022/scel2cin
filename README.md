# 搜狗词库转换工具

一个用于转换搜狗输入法词库(.scel)文件的图形界面工具，支持将词库转换为文本格式(.txt)和中文输入法通用格式(.cin)。

## 功能特点

- 图形界面操作，简单易用
- 支持单个词库文件转换
- 支持批量转换并合并多个词库
- 支持以下转换功能：
  - SCEL转TXT：将搜狗词库转换为文本格式
  - SCEL直接转CIN：将搜狗词库直接转换为CIN格式
  - TXT转CIN：将已转换的文本文件转换为CIN格式
  - 合并CIN：合并多个CIN文件，自动去重

## 安装说明

### 环境要求

- Python 3.6 或更高版本
- tkinter（Python标准库，通常随Python一起安装）

### 在Haiku系统中构建和安装

#### 构建环境要求

- Haiku操作系统
- Python 3.10或更高版本
- SCons构建工具

#### 构建步骤

1. 克隆或下载本项目到本地
2. 进入项目目录
3. 执行构建命令：
   ```bash
   scons
   ```
4. 构建hpkg包：
   ```bash
   scons package
   ```

#### 安装方法

构建完成后，你可以通过以下方式安装：

1. 通过HaikuDepot安装：
   - scel2cin 自动开HaikuDepot安装

2. 通过命令行安装：
   ```bash
   pkgman install scel2cin
   ```

## 使用方法

### 启动程序

在Haiku系统中，你可以通过以下方式启动程序：

1. 从应用程序菜单启动：
   - 打开应用程序菜单
   - 找到并点击"Scel2Cin"图标

2. 通过终端启动：
   ```bash
   Scel2Cin
   ```

### 主要功能说明

1. **转换SCEL**
   - 点击"浏览"选择一个.scel词库文件
   - 点击"转换SCEL"按钮
   - 选择保存位置，文件将被转换为.txt格式

2. **直接转换为CIN**
   - 点击"浏览"选择一个.scel词库文件
   - 点击"直接转换为CIN"按钮
   - 程序将直接生成对应的.cin文件

3. **批量转换并合并**
   - 点击"批量转换并合并"按钮
   - 选择多个.scel文件
   - 选择保存位置
   - 程序将所有词库合并转换为一个.cin文件，并自动去重

4. **TXT转CIN**
   - 点击"浏览"选择一个.txt格式的词库文件
   - 点击"TXT转CIN"按钮
   - 程序将生成对应的.cin文件

5. **合并CIN**
   - 点击"合并CIN"按钮
   - 选择多个.cin文件
   - 选择保存位置
   - 程序将所有词库合并为一个.cin文件，并自动去重